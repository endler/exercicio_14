# exercicio_14

Audio Record e Player

## Exercício 1

Modifique a aplicação para que seja permitido diversas gravações e que a cada gravação feita, seja adicionado um novo elemento em uma lista de gravações (Utilize ListView). Para isso, a cada gravação, armazene o resultado (path) em uma lista, paara que você tenha acesso a todas as gravações.

O usuário deverá ser capaz de clicar em um item da lista e o audio escolhido deve começar a tocar. 